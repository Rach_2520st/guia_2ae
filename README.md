Puerto Seco:

Para obtener el programa se debe clonar el presente repositorio, luego de esto ingresar  la carpeta del repositorio mediante terminal y luego ejecutar el comando "make".

Los elementos del arreglo bidimensional del puerto seco se ingresan manualmente por el usuario.

este programa contiene una clase que se encarga de agregar elementos, eliminar elementos y mostrar el puerto seco con diferentes funciones integradas.

Básicamente este programa está inspirado en el laboratorio anterior.
