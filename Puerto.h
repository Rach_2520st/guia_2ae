#include <iostream>
using namespace std;

#ifndef PUERTO_H
#define PUERTO_H

class Puerto
{
	private:
		int n;
		int m;
		int *puerto = NULL;
		int topem;
		int topen;
		bool vol;
		int dato;
		int largo;
		int ancho;
	public:
	
		Puerto(int m, int n);
		void puerto_lleno();
		void agregar(int dato);
		void puerto_vacio();
		void eliminar(int largo, int ancho);
		void Mostrar();
	
	
};
#endif